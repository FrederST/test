import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ListAgentsComponent } from 'src/app/components/agent/list-agents/list-agents.component';
import { Agent } from 'src/app/models/Agent';

import { AgentService } from './agent.service';

fdescribe('AgentService', () => {
  let service: AgentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{
        path: 'list-agents', component: ListAgentsComponent
      }]), HttpClientModule],
    });
    service = TestBed.inject(AgentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('create agent test', () => {
    const agent: Agent = { id: null, nameA: 'Test', lastNameA: 'Create', yearsExperienceA: 7, secretaryIdA: 1, trackIdA: 1 };
    service.createAgent(agent).subscribe((value) => {
      expect(value).not.toBeNull();
      agent.id = value.id;
      expect(value).toEqual(agent);
    });
  });

  it('edit agent test', () => {
    const agent: Agent = { id: 34, nameA: 'Edited', lastNameA: 'Edited', yearsExperienceA: 7, secretaryIdA: 1, trackIdA: 2 };
    service.editAgent(agent).subscribe((value) => {
      expect(value).not.toBeNull();
      expect(value).toEqual(agent);
    });
  });

  it('get agent test', () => {
    const agent: Agent = { id: 34, nameA: 'Edited', lastNameA: 'Edited', yearsExperienceA: 7, secretaryIdA: 1, trackIdA: 2 };
    service.showAgent(agent);
  });

  it('delete agent test', () => {
    const agent: Agent = { id: null, nameA: 'Edited', lastNameA: 'Edited', yearsExperienceA: 7, secretaryIdA: 1, trackIdA: 2 };
    service.createAgent(agent).subscribe((value) => {
      service.deleteAgent(value.id).subscribe((a) => {
        expect(a).toBeNull();
      });
    });
  });

});
