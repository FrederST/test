import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HistoryAssignation } from 'src/app/models/HistoryAssignation';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  urlHistory = environment.apiURL + '/history';

  constructor(private http: HttpClient) { }

  getHistoryOfAgent(agentId: number): Observable<HistoryAssignation[]> {
    return this.http.get<HistoryAssignation[]>(this.urlHistory + '/agent-and-tracks/' + agentId);
  }

  getHistoryOfTrack(trackId: number): Observable<HistoryAssignation[]> {
    return this.http.get<HistoryAssignation[]>(this.urlHistory + '/track-and-agents/' + trackId);
  }

  getAllHistory(): Observable<HistoryAssignation[]> {
    return this.http.get<HistoryAssignation[]>(this.urlHistory + '/all');
  }

}
