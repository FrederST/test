import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrackRoutingModule } from './track-routing.module';
import { ListTracksComponent } from 'src/app/components/track/list-tracks/list-tracks.component';
import { DataTablesModule } from 'angular-datatables';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateTrackComponent } from 'src/app/components/track/create-track/create-track.component';
import { HistoryTrackComponent } from 'src/app/components/track/history-track/history-track.component';


@NgModule({
  declarations: [
    ListTracksComponent,
    CreateTrackComponent,
    HistoryTrackComponent
  ],
  imports: [
    CommonModule,
    TrackRoutingModule,
    ReactiveFormsModule,
    DataTablesModule,
  ]
})
export class TrackModule { }
