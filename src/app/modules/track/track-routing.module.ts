import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateTrackComponent } from 'src/app/components/track/create-track/create-track.component';
import { HistoryTrackComponent } from 'src/app/components/track/history-track/history-track.component';
import { ListTracksComponent } from 'src/app/components/track/list-tracks/list-tracks.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ListTracksComponent },
      { path: 'create-track', component: CreateTrackComponent },
      { path: 'history-track', component: HistoryTrackComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrackRoutingModule { }
