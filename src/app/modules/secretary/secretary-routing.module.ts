import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateSecretaryComponent } from 'src/app/components/secetary/create-secretary/create-secretary.component';
import { ListSecretarysComponent } from 'src/app/components/secetary/list-secretarys/list-secretarys.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', component: ListSecretarysComponent},
      {path: 'create-secretary', component: CreateSecretaryComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecretaryRoutingModule { }
