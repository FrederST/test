import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecretaryRoutingModule } from './secretary-routing.module';
import { ListSecretarysComponent } from 'src/app/components/secetary/list-secretarys/list-secretarys.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { CreateSecretaryComponent } from 'src/app/components/secetary/create-secretary/create-secretary.component';


@NgModule({
  declarations: [
    ListSecretarysComponent,
    CreateSecretaryComponent
  ],
  imports: [
    CommonModule,
    SecretaryRoutingModule,
    DataTablesModule,
    ReactiveFormsModule
  ]
})
export class SecretaryModule { }
