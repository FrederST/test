import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateAgentComponent } from 'src/app/components/agent/create-agent/create-agent.component';
import { HistoryAgentComponent } from 'src/app/components/agent/history-agent/history-agent.component';
import { ListAgentsComponent } from 'src/app/components/agent/list-agents/list-agents.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ListAgentsComponent },
      { path: 'create-agent', component: CreateAgentComponent },
      { path: 'history-agent', component: HistoryAgentComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentRoutingModule { }
