import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgentRoutingModule } from './agent-routing.module';
import { ListAgentsComponent } from 'src/app/components/agent/list-agents/list-agents.component';
import { DataTablesModule } from 'angular-datatables';
import { CreateAgentComponent } from 'src/app/components/agent/create-agent/create-agent.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HistoryAgentComponent } from 'src/app/components/agent/history-agent/history-agent.component';


@NgModule({
  declarations: [
    ListAgentsComponent,
    CreateAgentComponent,
    HistoryAgentComponent
  ],
  imports: [
    CommonModule,
    AgentRoutingModule,
    ReactiveFormsModule,
    DataTablesModule,
  ]
})
export class AgentModule { }
