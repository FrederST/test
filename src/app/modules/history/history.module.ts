import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoryRoutingModule } from './history-routing.module';
import { HistorysComponent } from 'src/app/components/history/historys/historys.component';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  declarations: [
    HistorysComponent
  ],
  imports: [
    CommonModule,
    HistoryRoutingModule,
    DataTablesModule,
  ]
})
export class HistoryModule { }
