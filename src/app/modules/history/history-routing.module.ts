import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistorysComponent } from 'src/app/components/history/historys/historys.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: HistorysComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryRoutingModule { }
