import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryTrackComponent } from './history-track.component';

import {DataTablesModule} from 'angular-datatables';

describe('HistoryTrackComponent', () => {
  let component: HistoryTrackComponent;
  let fixture: ComponentFixture<HistoryTrackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryTrackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryTrackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
