import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { HistoryAssignation } from 'src/app/models/HistoryAssignation';
import { HistoryService } from 'src/app/services/history/history.service';

@Component({
  selector: 'app-history-track',
  templateUrl: './history-track.component.html',
  styleUrls: ['./history-track.component.css']
})
export class HistoryTrackComponent implements OnInit {

  dbHistory: HistoryAssignation[];
  dtOptions: DataTables.Settings = { pageLength: 5 };
  dtTrigger: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private historyService: HistoryService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.params.track !== undefined) {
      this.historyService.getHistoryOfTrack(this.activatedRoute.snapshot.params.track).subscribe((history) => {
        this.dbHistory = history;
        this.dtTrigger.next();
      });
    }
  }

  tracks(): void {
    this.router.navigate(['track']);
  }

}
