import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Track } from 'src/app/models/Track';
import { DialogService } from 'src/app/services/share/dialog.service';
import { TrackService } from 'src/app/services/track/track.service';

@Component({
  selector: 'app-list-tracks',
  templateUrl: './list-tracks.component.html',
  styleUrls: ['./list-tracks.component.css']
})
export class ListTracksComponent implements OnInit {

  dbTracks: Track[];
  dtOptions: DataTables.Settings = { pageLength: 5 };
  dtTrigger: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private trackService: TrackService,
    private dialogService: DialogService) { }

  ngOnInit(): void {
    this.trackService.getAllTracks().subscribe((tracks) => {
      this.dbTracks = tracks;
      this.dtTrigger.next();
    }, (error) => {
      console.log(error);
    });
  }

  createTrack(): void {
    this.router.navigate(['track', 'create-track']);
  }

  historyTrack(trackId: number): void {
    this.router.navigate(['track', 'history-track', { track: trackId }]);
  }

  editTrack(track: Track): void {
    this.router.navigate(['track', 'create-track', { editTrack: JSON.stringify(track) }]);
  }

  deleteTrack(trackId: number, index: number): void {

    this.dialogService.confirm('Eliminar',
      'Seguro que desea eliminar esta Vía ?', 'Seguro', 'Cancelar').then((confirm) => {
        if (confirm) {
          this.trackService.deleteTrack(trackId).subscribe(() => {
            this.dialogService.showMessage('Mensaje', 'Vía Eliminada');
            this.dbTracks.splice(index, 1);
          }, (error) => {
            console.log(error);
          });
        }
      });

  }

}
