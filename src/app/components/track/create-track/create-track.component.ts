import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Track } from 'src/app/models/Track';
import { DialogService } from 'src/app/services/share/dialog.service';
import { TrackService } from 'src/app/services/track/track.service';

@Component({
  selector: 'app-create-track',
  templateUrl: './create-track.component.html',
  styleUrls: ['./create-track.component.css']
})
export class CreateTrackComponent implements OnInit {

  createTrackForm: FormGroup;

  isEditable = false;
  track: Track = { id: null, typeT: null, streetOrCarrerT: null, numberT: null, levelCongestionT: null };
  constructor(
    private router: Router,
    private trackService: TrackService,
    private activatedRoute: ActivatedRoute,
    private dialogService: DialogService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {

    this.createTrackForm = this.formBuilder.group({
      typeT: new FormControl('', [Validators.required]),
      streetOrCarrerT: new FormControl('', [Validators.required]),
      numberT: new FormControl('', [Validators.required]),
      levelCongestionT: new FormControl('', [Validators.required])
    });

    if (this.activatedRoute.snapshot.params.editTrack !== undefined) {
      this.createTrackForm.patchValue(JSON.parse(this.activatedRoute.snapshot.params.editTrack));
      this.track = JSON.parse(this.activatedRoute.snapshot.params.editTrack);
      this.isEditable = true;
    }

  }

  createTrack(): void {
    this.track = this.createTrackForm.value;
    this.trackService.createTrack(this.track).subscribe(() => {
      this.dialogService.showMessage('Mensaje', 'Vía Creada');
      this.router.navigate(['track']);
    }, (error) => {
      console.log(error);
    });
  }

  editTrack(): void {
    this.track = this.createTrackForm.value;
    this.track.id = JSON.parse(this.activatedRoute.snapshot.params.editTrack).id;

    this.trackService.editTrack(this.track).subscribe(() => {
      this.dialogService.showMessage('Mensaje', 'Vía Editada');
      this.router.navigate(['track']);
    }, (error) => {
      console.log(error);
    });
  }

}
