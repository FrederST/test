import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Agent } from 'src/app/models/Agent';
import { Secretary } from 'src/app/models/Secretary';
import { Track } from 'src/app/models/Track';
import { AgentService } from 'src/app/services/agent/agent.service';
import { SecretaryService } from 'src/app/services/secretary/secretary.service';
import { TrackService } from 'src/app/services/track/track.service';
import { ResponseMessage } from 'src/app/models/ResponseMessage';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogService } from 'src/app/services/share/dialog.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-agent',
  templateUrl: './create-agent.component.html',
  styleUrls: ['./create-agent.component.css']
})
export class CreateAgentComponent implements OnInit {

  createAgentForm: FormGroup;

  errorMessage: ResponseMessage = { timestamp: null, message: null, details: null };
  dbSecretarys: Observable<Secretary[]>;
  dbTracks: Observable<Track[]>;
  isEditable = false;

  agent: Agent = { id: null, nameA: null, lastNameA: null, yearsExperienceA: null, secretaryIdA: null, trackIdA: null };

  constructor(
    private trackService: TrackService,
    private secretaryService: SecretaryService,
    private agentService: AgentService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialogService: DialogService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createAgentForm = this.formBuilder.group({
      nameA: new FormControl('', [Validators.required]),
      lastNameA: new FormControl('', [Validators.required]),
      years: new FormControl(0, [Validators.required, Validators.min(0), Validators.max(40)]),
      months: new FormControl(0, [Validators.required, Validators.min(0), Validators.max(12)]),
      secretaryIdA: new FormControl('', [Validators.required]),
      trackIdA: new FormControl('', [Validators.required])
    });

    if (this.activatedRoute.snapshot.params.editAgent !== undefined) {
      const tempAgent = JSON.parse(this.activatedRoute.snapshot.params.editAgent);
      this.createAgentForm.patchValue(tempAgent);
      this.setYearsAndMonths(tempAgent.yearsExperienceA);
      this.isEditable = true;
    }

    this.dbSecretarys = this.secretaryService.getAllSecretarys();

    this.dbTracks = this.trackService.getAllAllowedTracks();
  }

  createAgent(): void {
    this.agent = this.createAgentForm.value;
    this.agent.yearsExperienceA = this.createAgentForm.value.years + this.createAgentForm.value.months / 12;

    this.agentService.createAgent(this.agent).subscribe(() => {
      this.dialogService.showMessage('Mensaje', 'Agente Guardado');
      this.router.navigate(['agent']);
    }, (error) => {
      alert(error);
    });
  }

  editAgent(): void {
    this.agent = this.createAgentForm.value;
    this.agent.yearsExperienceA = this.createAgentForm.value.years + this.createAgentForm.value.months / 12;
    this.agent.id = JSON.parse(this.activatedRoute.snapshot.params.editAgent).id;

    this.agentService.editAgent(this.agent).subscribe(() => {
      this.dialogService.showMessage('Mensaje', 'Agente Editado');
      this.router.navigate(['list-agents']);
    }, (error) => {
      this.errorMessage = {
        timestamp: error.error.timestamp,
        message: error.error.message,
        details: error.error.details
      };
      this.dialogService.showMessage('Advertencia', this.errorMessage.message);
    });
  }

  private setYearsAndMonths(yearsExperience: number): void {
    const yearsFloat = parseFloat(yearsExperience.toString());

    this.createAgentForm.patchValue({
      years: yearsFloat.toFixed()
    });

    if (yearsFloat.toString().split('.')[1] !== undefined) {
      const decimal = parseFloat('0.' + (yearsFloat + '').split('.')[1]) * 12;
      this.createAgentForm.patchValue({
        months: Math.round(Number(decimal))
      });
    }
  }

}
