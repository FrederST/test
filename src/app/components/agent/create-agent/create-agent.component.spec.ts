import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Agent } from 'src/app/models/Agent';
import { AgentService } from 'src/app/services/agent/agent.service';
import { ListAgentsComponent } from '../list-agents/list-agents.component';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import { CreateAgentComponent } from './create-agent.component';

fdescribe('CreateAgentComponent', () => {
  let httpTestingController: HttpTestingController;
  let agentService: AgentService;
  let component: CreateAgentComponent;
  let fixture: ComponentFixture<CreateAgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreateAgentComponent],
      imports: [RouterTestingModule.withRoutes([{
        path: 'list-agents', component: ListAgentsComponent
      }]), HttpClientModule, ReactiveFormsModule, HttpClientTestingModule],
      providers: [AgentService]
    })
      .compileComponents();
    httpTestingController = TestBed.inject(HttpTestingController);
    agentService = TestBed.inject(AgentService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAgentComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test form validity', () => {
    const form = component.createAgentForm;
    expect(form.valid).toBeFalsy();

    const nameInput = form.controls.nameA;
    nameInput.setValue('John');
    expect(form.valid).toBeFalsy();

    const lastNameInput = form.controls.lastNameA;
    lastNameInput.setValue('Peter');
    expect(form.valid).toBeFalsy();

    const years = form.controls.years;
    years.setValue(5);
    expect(form.valid).toBeFalsy();

    const months = form.controls.months;
    months.setValue(6);
    expect(form.valid).toBeFalsy();

    const secretary = form.controls.secretaryIdA;
    secretary.setValue(1);
    expect(form.valid).toBeFalsy();

    const track = form.controls.trackIdA;
    track.setValue(2);
    expect(form.valid).toBeTruthy();

  });

  it('submitting a form emits a user', () => {
    const form = component.createAgentForm;
    expect(form.valid).toBeFalsy();

    form.controls.nameA.setValue('Andres');
    form.controls.lastNameA.setValue('Mariano');
    form.controls.years.setValue(5);
    form.controls.months.setValue(6);
    form.controls.secretaryIdA.setValue(2);
    form.controls.trackIdA.setValue(2);
    expect(form.valid).toBeTruthy();

    // Trigger the login function
    component.createAgent();
  });

});
