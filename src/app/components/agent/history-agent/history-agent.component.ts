import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HistoryService } from 'src/app/services/history/history.service';
import { HistoryAssignation } from 'src/app/models/HistoryAssignation';

@Component({
  selector: 'app-history-agent',
  templateUrl: './history-agent.component.html',
  styleUrls: ['./history-agent.component.css']
})
export class HistoryAgentComponent implements OnInit {

  dbHistory: HistoryAssignation[];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private historyService: HistoryService) { }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.params.agent !== undefined) {
      console.log(this.activatedRoute.snapshot.params.agent);
      this.historyService.getHistoryOfAgent(this.activatedRoute.snapshot.params.agent).subscribe((history) => {
        this.dbHistory = history;
      }, (error) => {
        console.log(error);
      });
    }
  }

  agents(): void {
    this.router.navigate(['agent']);
  }

}
