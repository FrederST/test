import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryAgentComponent } from './history-agent.component';

describe('HistoryAgentComponent', () => {
  let component: HistoryAgentComponent;
  let fixture: ComponentFixture<HistoryAgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryAgentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
