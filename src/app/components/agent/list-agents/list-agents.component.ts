import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Agent } from 'src/app/models/Agent';
import { AgentService } from 'src/app/services/agent/agent.service';
import { DialogService } from 'src/app/services/share/dialog.service';

@Component({
  selector: 'app-list-agents',
  templateUrl: './list-agents.component.html',
  styleUrls: ['./list-agents.component.css']
})
export class ListAgentsComponent implements OnInit {

  dbAgents: Agent[];
  dtOptions: DataTables.Settings = { pageLength: 5 };
  dtTrigger: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private agentService: AgentService,
    private dialogService: DialogService) { }

  ngOnInit(): void {

    this.agentService.getAllAgents().subscribe((agents) => {
      this.dbAgents = agents;
      this.dtTrigger.next();
    }, (error) => {
      console.log(error);
    });

  }

  agents(): void {
    this.router.navigate(['agent']);
  }


  showAgent(agent: Agent): void {
    this.agentService.showAgent(agent);
  }

  createAgent(): void {
    this.router.navigate(['agent', 'create-agent']);
  }

  historyAgent(agetnId: number): void{
    this.router.navigate(['agent', 'history-agent', { agent: agetnId }]);
  }

  editAgent(agent: Agent): void {
    this.router.navigate(['agent', 'create-agent', { editAgent: JSON.stringify(agent) }]);
  }

  deleteAgent(agentId: number, index: number): void {
    this.dialogService.confirm('Eliminar',
      'Seguto que desea eliminar este agente ?',
      'Seguro', 'Cancelar').then((confirmed) => {
        if (confirmed) {
          this.agentService.deleteAgent(agentId).subscribe(() => {
            this.dialogService.showMessage('Mensaje', 'Agente Eliminado', 'Ok');
            this.dbAgents.splice(index, 1);
          }, (error) => {
            console.log(error);
          });
        }
      });
  }

}
