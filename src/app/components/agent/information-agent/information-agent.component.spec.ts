import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationAgentComponent } from './information-agent.component';

describe('InformationAgentComponent', () => {
  let component: InformationAgentComponent;
  let fixture: ComponentFixture<InformationAgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformationAgentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
