import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSecretarysComponent } from './list-secretarys.component';

describe('ListSecretarysComponent', () => {
  let component: ListSecretarysComponent;
  let fixture: ComponentFixture<ListSecretarysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSecretarysComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSecretarysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
