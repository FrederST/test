import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Secretary } from 'src/app/models/Secretary';
import { SecretaryService } from 'src/app/services/secretary/secretary.service';
import { DialogService } from 'src/app/services/share/dialog.service';

@Component({
  selector: 'app-list-secretarys',
  templateUrl: './list-secretarys.component.html',
  styleUrls: ['./list-secretarys.component.css']
})
export class ListSecretarysComponent implements OnInit {
  dbSecretarys: Secretary[];
  dtOptions: DataTables.Settings = { pageLength: 5 };
  dtTrigger: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private secretaryService: SecretaryService,
    private dialogService: DialogService) { }

  ngOnInit(): void {
    this.secretaryService.getAllSecretarys().subscribe((secretary) => {
      this.dbSecretarys = secretary;
      this.dtTrigger.next();
    }, (error) => {
      console.log(error);
    });
  }

  createSecretary(): void {
    this.router.navigate(['secretary', 'create-secretary']);
  }

  editSecretary(secretary: Secretary): void {
    this.router.navigate(['secretary', 'create-secretary', { editSecretary: JSON.stringify(secretary) }]);
  }

  deleteSecretary(secretaryId: number, index: number): void {
    this.dialogService.confirm('Eliminar',
    'Seguro/a que desea elminar esta secretaria ?', 'Seguro/a', 'Cancelar').then((confirm) => {
      if (confirm){
        this.secretaryService.deleteTrack(secretaryId).subscribe(() => {
          this.dialogService.showMessage('Mesaje', 'Secretaria Eliminada');
        }, (error) => {
          console.log(error);
        });
      }
    });
  }

}
