import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSecretaryComponent } from './create-secretary.component';

describe('CreateSecretaryComponent', () => {
  let component: CreateSecretaryComponent;
  let fixture: ComponentFixture<CreateSecretaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSecretaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSecretaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
