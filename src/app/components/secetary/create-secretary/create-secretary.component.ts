import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Secretary } from 'src/app/models/Secretary';
import { SecretaryService } from 'src/app/services/secretary/secretary.service';
import { DialogService } from 'src/app/services/share/dialog.service';

@Component({
  selector: 'app-create-secretary',
  templateUrl: './create-secretary.component.html',
  styleUrls: ['./create-secretary.component.css']
})
export class CreateSecretaryComponent implements OnInit {

  createSecretaryForm: FormGroup;

  isEditable = false;
  secretary: Secretary = { id: null, nameS: null };
  constructor(
    private router: Router,
    private secretaryService: SecretaryService,
    private activatedRoute: ActivatedRoute,
    private dialogService: DialogService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {

    this.createSecretaryForm = this.formBuilder.group({
      nameS: new FormControl('', [Validators.required])
    });

    if (this.activatedRoute.snapshot.params.editSecretary !== undefined) {
      this.createSecretaryForm.patchValue(JSON.parse(this.activatedRoute.snapshot.params.editSecretary));
      this.secretary = JSON.parse(this.activatedRoute.snapshot.params.editSecretary);
      this.isEditable = true;
    }

  }

  createSecretary(): void {
    this.secretary = this.createSecretaryForm.value;
    this.secretaryService.createSecretary(this.secretary).subscribe(() => {
      this.dialogService.showMessage('Mensaje', 'Secretaria Creada Satisfactoriamente');
      this.router.navigate(['secretary']);
    }, (error) => {
      console.log(error);
    });
  }

  editSecretary(): void {
    this.secretary = this.createSecretaryForm.value;
    this.secretary.id = JSON.parse(this.activatedRoute.snapshot.params.editSecretary).id;

    this.secretaryService.editSecretary(this.secretary).subscribe(() => {
      this.dialogService.showMessage('Mensaje', 'Secretaria Editada Satisfactoriamente');
      this.router.navigate(['secretary']);
    }, (error) => {
      console.log(error);
    });
  }

}
