import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'quileia-app';

  constructor(private router: Router) { }

  agents(): void {
    this.router.navigate(['agent']);
  }

  tracks(): void {
    this.router.navigate(['track']);
  }

  historys(): void {
    this.router.navigate(['historys']);
  }

  secretarys(): void {
    this.router.navigate(['secretary']);
  }

}
