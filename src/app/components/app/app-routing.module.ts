import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'agent',
    loadChildren: () => import('src/app/modules/agent/agent.module').then(mod => mod.AgentModule)
  },
  {
    path: 'track',
    loadChildren: () => import('src/app/modules/track/track.module').then(mod => mod.TrackModule)
  },
  {
    path: 'historys',
    loadChildren: () => import('src/app/modules/history/history.module').then(mod => mod.HistoryModule)
  },
  {
    path: 'secretary',
    loadChildren: () => import('src/app/modules/secretary/secretary.module').then(mod => mod.SecretaryModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
