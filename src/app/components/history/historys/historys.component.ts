import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { HistoryAssignation } from 'src/app/models/HistoryAssignation';
import { HistoryService } from 'src/app/services/history/history.service';

@Component({
  selector: 'app-historys',
  templateUrl: './historys.component.html',
  styleUrls: ['./historys.component.css']
})
export class HistorysComponent implements OnInit {

  dbHistory: HistoryAssignation[];
  dtOptions: DataTables.Settings = { pageLength: 7 };
  dtTrigger: Subject<any> = new Subject();

  constructor(private historyService: HistoryService) { }

  ngOnInit(): void {
    this.historyService.getAllHistory().subscribe((history) => {
      this.dbHistory = history;
      this.dtTrigger.next();
    });
  }

}
