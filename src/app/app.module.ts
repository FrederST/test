import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './components/app/app-routing.module';
import { AppComponent } from './components/app/app.component';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmationDialogComponent } from './components/share/confirmation-dialog/confirmation-dialog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MessageDialogComponent } from './components/share/message-dialog/message-dialog.component';
import { InformationAgentComponent } from './components/agent/information-agent/information-agent.component';

@NgModule({
  declarations: [
    AppComponent,
    ConfirmationDialogComponent,
    MessageDialogComponent,
    InformationAgentComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule,
  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
