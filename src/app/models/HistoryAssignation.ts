export interface HistoryAssignation {
    id: number;
    agentIdH: number;
    agentNameH: string;
    trackIdH: number;
    trackNameH: string;
    startDateH: number;
    finalDateH: number;
    activeH: boolean;
}
