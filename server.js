const express = require('express');
const path = require('path');

const app = express();

app.use(express.static(__dirname+'/dist/quileia-app'));
app.get('/',function(req,res){
    res.sendFile(path.join(__dirname+'/dist/quileia-app/index.html'));
});

app.listen(process.env.PORT || 8080);